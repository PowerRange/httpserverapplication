#from markupsafe import escape
from datetime import datetime
from flask import Flask, request, jsonify
import os

app = Flask(__name__)


@app.route('/')
def hello():
    return '<h1>Hello, World!</h1>'


@app.route('/about/')
def about():
    return '<h3>This is a Flask web application.</h3>'
    
#@app.route('/echo', methods=['GET','POST']) 
#def echo():
#    
#    data = request.json
#   return jsonify(data)


@app.route('/echo', methods=['GET','POST'])
def form_to_json():
    data = request.form.to_dict(flat=False)
    return jsonify(data)
    
@app.route('/datetime')
def home():
    date = datetime.now()
    return str(date)
   
   
@app.route('/ping')    

def check_ping():
    hostname = "localhost"
    response = os.system("ping -n 1 " + hostname)
    # check the response
    if response == 200:
        pingstatus = "Network Active"
    else:
        pingstatus = "Network Error"

    return pingstatus#from markupsafe import escape
from datetime import datetime
from flask import Flask, request, jsonify
import os

app = Flask(__name__)


@app.route('/')
def hello():
    return '<h1>Hello, World!</h1>'


@app.route('/about/')
def about():
    return '<h3>This is a Flask web application.</h3>'
    
#@app.route('/echo', methods=['GET','POST']) 
#def echo():
#    
#    data = request.json
#   return jsonify(data)


@app.route('/echo', methods=['GET','POST'])
def form_to_json():
    data = request.form.to_dict(flat=False)
    return jsonify(data)
    
@app.route('/datetime')
def home():
    date = datetime.now()
    return str(date)
   
   
@app.route('/ping')    

def check_ping():
    hostname = "localhost"
    response = os.system("ping -n 1 " + hostname)
    # and then check the response...
    if response == 200:
        pingstatus = "Network Active"
    else:
        pingstatus = "Network Error"

    return pingstatus